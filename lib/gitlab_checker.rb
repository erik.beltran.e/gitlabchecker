require "gitlab_checker/version"
require "http"
require "time"
require "logger"


module GitlabChecker
  # Your code goes here...
  	@site='https://gitlab.com/'
  	@seconds=6
  	@times=10
  	@code = 0
  	@logger

	class << self
		attr_accessor :code
	end

  	def self.init
		@logger = Logger.new(STDOUT)
		@logger.level = (ENV["STAGE"]=="development") ? Logger::DEBUG : Logger::WARN
  	end
  	self.init

  	def self.site_is_ok?
  		code = get_code
		return (code == 200) ?  true : false
	end

  	def self.time_response
		time_start = Time.now
		get_code
		time_end = Time.now
		time_result = time_end - time_start
		@logger.info("time_result: #{time_result}")
		return time_result
	end

	def self.get_average
		time_average = 0
		for attempt in 1..@times do
			@logger.info("attempt number: #{attempt}")
			attemp_response = time_response			
			time_average += attemp_response
		end
		average_result=time_average/@times
		@logger.info("average_result #{average_result}")
		return average_result
	end

	def self.get_code(site=@site)
		response = HTTP.get(site)
		code = response.code
  		@code = code
   		if code == 301 || code == 302
   			url_to_redirect = response.headers["location"]
			return get_code(url_to_redirect)
		else
			code
		end
	end
end
