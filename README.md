# GitlabChecker

Is a project to check the status of gitlab.com | about.gitlab.com and get the time response

RUBY version: 2.4

## Building

* git clone https://gitlab.com/erik.beltran.e/gitlabchecker.git
* cd gitlabchecker
* bundle install 
* rake install

## Development
to view some logs (INFO level)  , you can set an enviroment variable named STAGE as development

```bash
$ export STAGE=development
```
or remove it if you dont want look the logs

```bash
unset STAGE
```

## Usage

This app, dont have any parameters (maybe in the next version ;) ), just execute it!


```bash
$ gitlab_checker 
```

and feel the magic of the internet...