require_relative "./test_helper"

class GitlabCheckerTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::GitlabChecker::VERSION
  end

  def test_site_status
  	assert_equal true, GitlabChecker.site_is_ok?
  end

  def test_get_time_response
  	assert GitlabChecker.time_response > 0
  end

  def test_average_response
  	assert GitlabChecker.get_average > 0
  end

  def test_get_code
  	assert GitlabChecker.get_code > 0
  end
end