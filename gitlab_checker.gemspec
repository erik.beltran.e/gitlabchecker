
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "gitlab_checker/version"

Gem::Specification.new do |spec|
  spec.name          = "gitlab_checker"
  spec.version       = GitlabChecker::VERSION
  spec.authors       = ["Erik Beltran"]
  spec.email         = ["erik.beltran.e@gmail.com"]

  spec.summary       = "Gitlab Checker status and average time response."
  spec.description   = "just a gem to check the gitlab site status and the time average , its a CLI tool and LIB."
  spec.homepage      = "http://beltran.pw/"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "http://localhost"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "bin"
  spec.executables   = ["gitlab_checker"] #spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_dependency 'http', '~> 3.0.0'
  spec.add_dependency 'logger', '~> 1.2'
end
